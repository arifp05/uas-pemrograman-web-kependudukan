<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kk extends Model
{
    protected $table = 'kk';

    public $timestamps = true;

    protected $guarded = [];

    public function kepala()
    {
        return $this->hasOne('App\Model\Penduduk', 'nik', 'nik');
    }
}
