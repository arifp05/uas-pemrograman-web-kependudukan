<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kk;

class CariKkController extends Controller
{
    public function index(Request $request)
    {
    	return view("kk.cari");
    }

    public function result(Request $request)
    {
    	$input = $request->all();
    	$kk = Kk::where("nikk", "LIKE", "%".$input["nikk"]."%")->get();	
    	$pencarian = $input["nikk"];

    	return view("kk.hasil", compact(["kk","pencarian"]));
    }
}
