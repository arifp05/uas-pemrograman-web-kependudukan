<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Penduduk;
use App\Model\Kk;

class DaftarKependudukanController extends Controller
{
    public function index(Request $request)
    {
    	$penduduk = Penduduk::all();

    	return view("kependudukan.index", compact(["penduduk"]));
    }

    public function input(Request $request)
    {
    	$kk = Kk::all();

    	return view("kependudukan.input", compact(["kk"]));
    }

    public function insert(Request $request)
    {
    	$input = $request->all();

    	$penduduk = new Penduduk;
    	$penduduk->nik = $input["nik"];
    	if(isset($input["nikk"])){
    		$penduduk->nikk = $input["nikk"];
    	}
    	$penduduk->nama = $input["nama"];
    	$penduduk->jenis_kelamin = $input["jenis_kelamin"];
    	$penduduk->tempat_lahir = $input["tempat_lahir"];
    	$penduduk->tgl_lahir = $input["tgl_lahir"];
    	$penduduk->alamat = $input["alamat"];
    	$penduduk->rt_rw = $input["rt_rw"];
    	$penduduk->kelurahan = $input["kelurahan"];
    	$penduduk->kecamatan = $input["kecamatan"];
    	$penduduk->agama = $input["agama"];
    	$penduduk->status_perkawinan = $input["status_perkawinan"];
    	$penduduk->pekerjaan = $input["pekerjaan"];
    	$penduduk->kewarganegaraan = $input["kewarganegaraan"];
    	$penduduk->berlaku_hingga = $input["berlaku_hingga"];
    	$penduduk->save();

    	return redirect("kependudukan");
    }

    public function edit(Request $request, $id)
    {
    	$input = $request->all();
    	$penduduk = Penduduk::where("nik", $id)->first();
    	$kk = Kk::all();

    	return view("kependudukan.edit", compact(["penduduk", "kk"]));
    }

    public function update(Request $request)
    {
    	$input = $request->all();
    	$penduduk = Penduduk::find($input["id"]);
    	$penduduk->nik = $input["nik"];
    	if(isset($input["nikk"])){
    		$penduduk->nikk = $input["nikk"];
    	}
    	$penduduk->nama = $input["nama"];
    	$penduduk->jenis_kelamin = $input["jenis_kelamin"];
    	$penduduk->tempat_lahir = $input["tempat_lahir"];
    	$penduduk->tgl_lahir = $input["tgl_lahir"];
    	$penduduk->alamat = $input["alamat"];
    	$penduduk->rt_rw = $input["rt_rw"];
    	$penduduk->kelurahan = $input["kelurahan"];
    	$penduduk->kecamatan = $input["kecamatan"];
    	$penduduk->agama = $input["agama"];
    	$penduduk->status_perkawinan = $input["status_perkawinan"];
    	$penduduk->pekerjaan = $input["pekerjaan"];
    	$penduduk->kewarganegaraan = $input["kewarganegaraan"];
    	$penduduk->berlaku_hingga = $input["berlaku_hingga"];
    	$penduduk->save();

    	return redirect("kependudukan");
    }

    public function delete(Request $request)
    {
    	$input = $request->all();

    	$penduduk = Penduduk::where("nik", $input["nik"])->first();
    	$penduduk->delete();

    	return redirect("kependudukan");
    }
}
