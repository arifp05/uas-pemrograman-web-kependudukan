<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Penduduk;

class CariKependudukanController extends Controller
{
    public function index(Request $request)
    {
    	return view("kependudukan.cari");
    }

    public function result(Request $request)
    {
    	$input = $request->all();
    	$penduduk = Penduduk::where("nik", "LIKE", "%".$input["nik"]."%")->get();	
    	$pencarian = $input["nik"];

    	return view("kependudukan.hasil", compact(["penduduk","pencarian"]));
    }
}
