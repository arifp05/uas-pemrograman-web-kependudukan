<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kk;
use App\Model\Penduduk;

class DaftarKkController extends Controller
{
    public function index(Request $request)
    {
    	$kk = Kk::all();

    	return view("kk.index", compact(["kk"]));
    }

    public function input(Request $request)
    {
    	$penduduk = Penduduk::all();
    	return view("kk.input", compact(["penduduk"]));
    }

    public function insert(Request $request)
    {
    	$input = $request->all();

    	$kk = new Kk;
    	$kk->nikk = $input["nikk"];
    	$kk->nik = $input["nik"];
    	$kk->save();

    	return redirect("kk");
    }

    public function edit(Request $request, $id)
    {
    	$kk = Kk::where("nikk", $id)->first();
    	$penduduk = Penduduk::all();

    	return view("kk.edit", compact(["kk","penduduk"]));
    }

    public function update(Request $request)
    {
    	$input = $request->all();

    	$kk = Kk::find($input["id"]);
    	$kk->nikk = $input["nikk"];
    	$kk->nik = $input["nik"];
    	$kk->save();

    	return redirect("kk");
    }

    public function delete(Request $request)
    {
    	$input = $request->all();
    	$kk = Kk::where("nikk", $input["id"]);
    	$kk->delete();

    	$penduduk = Penduduk::where("nikk", $input["id"]);
    	$penduduk->delete();

    	return redirect("kk");
    }

    public function detail(Request $request, $id)
    {
    	$penduduk = Penduduk::where("nikk", $id)->get();
    	return view("kk.detail", compact(["penduduk","id"]));
    }
}
