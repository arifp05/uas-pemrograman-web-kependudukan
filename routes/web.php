<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.index');
});

Route::get("/layout", function(){
	return view("layouts.application");
});

Route::group(["prefix" => "kk"], function(){
	Route::get("/", "DaftarKkController@index");
	Route::get("/input", "DaftarKkController@input");
	Route::post("/insert", "DaftarKkController@insert");
	Route::get("/edit/{id}", "DaftarKkController@edit");
	Route::get("/detail/{id}", "DaftarKkController@detail");
	Route::post("/update", "DaftarKkController@update");
	Route::post("/delete", "DaftarKkController@delete");
});

Route::group(["prefix" => "kependudukan"], function(){
	Route::get("/", "DaftarKependudukanController@index");
	Route::get("/input", "DaftarKependudukanController@input");
	Route::post("/insert", "DaftarKependudukanController@insert");
	Route::get("/edit/{id}", "DaftarKependudukanController@edit");
	Route::post("/update", "DaftarKependudukanController@update");
	Route::post("/delete", "DaftarKependudukanController@delete");
});

Route::group(["prefix" => "carikk"], function(){
	Route::get("/", "CariKkController@index");
	Route::post("/result", "CariKkController@result");
});

Route::group(["prefix" => "carinik"], function(){
	Route::get("/", "CariKependudukanController@index");
	Route::post("/result", "CariKependudukanController@result");
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
