@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Cari Nomor Induk Kependudukan</h4>
                <!-- <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p> -->
            </div>
            <div class="card-body ">
                <form action="{{url('carinik/result')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Nomor Induk Kependudukan</label>
                        <input type="text" name="nik" class="form-control" required="" value='{{$pencarian}}'>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary pull-right" value="Cari">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Hasil Pencarian {{$pencarian}}</h4>
                <!-- <p class="card-category">Daftar Penduduk beserta informasi lengkapnya</p> -->
            </div>
            <div class="card-body ">
                <div class="card-body table-full-width table-responsive">
                   
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nomor KK</th>
                            <th>NIK</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <!-- <th>Aksi</th> -->
                        </thead>
                        <tbody>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($penduduk as $p)
                            
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$p->nama}}</td>
                                <td>{{$p->nikk}}</td>
                                <td>{{$p->nik}}</td>
                                <td>{{$p->tempat_lahir}}</td>
                                <td>{{date('d-m-Y', strtotime($p->tgl_lahir))}}</td>
                                <td>{{$p->alamat}}</td>
                                <!-- <td><a href="{{url('kependudukan/edit')}}/{{$p->nik}}" class=" btn-warning btn-xs"><i class="fa fa-pencil" style="color: white"></i></a>&nbsp;<button type="button" class="open-del btn-danger btn-xs" data-id="{{$p->nik}}"><i class="fa fa-trash" style="color: white"></i></button></td> -->
                            </tr>
                            @php
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection

@section("modals")
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->

            <div class="modal-header">
                <h4 class="modal-title">Hapus Penduduk</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form method="post" action="{{url('kependudukan/delete')}}">
                <!-- Modal body -->
                {!! csrf_field() !!}
                <div class="modal-body">
                Apakah Anda yakin akan menghapus Penduduk ini ?
                <input type="hidden" name="nik" id="nik_modal">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script type="text/javascript">
    $(document).ready(function(){
        $(".open-del").click(function(){
            var id = $(this).data("id");
            $("#myModal").modal("show");
            $("#nik_modal").val(id);
        });
    });

function deleteModal(id){
    
    $("#myModal").modal("show");
    $("#nik_modal").val(id);
}
</script>
@endsection