@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Input Kartu Keluarga</h4>
                <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p>
            </div>
            <div class="card-body ">
                <form method="post" action="{{ url('kependudukan/update') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{$penduduk->id}}">
                    <div class="form-group">
                        <label>Nomor KK</label>
                        <select name="nikk" class="form-control">
                            <option selected="" disabled="">-- Pilih Nomor KK --</option>
                            @foreach($kk as $k)
                                <option {!! ($penduduk->nikk == $k->nikk) ? 'selected' : '' !!} value="{{$k->nikk}}">{{$k->nikk}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>NIK</label>
                        <input type="text" name="nik" class="form-control" value="{{$penduduk->nik}}">
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" value="{{$penduduk->nama}}">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label><br>
                        <input type="radio" name="jenis_kelamin" value="L" class="" {!! ($penduduk->jenis_kelamin == 'L') ? 'checked' : '' !!}> Laki-Laki<br>
                        <input type="radio" name="jenis_kelamin" value="P" class="" {!! ($penduduk->jenis_kelamin == 'P') ? 'checked' : '' !!}> Perempuan
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control" value="{{$penduduk->tempat_lahir}}">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control" value="{{$penduduk->tgl_lahir}}">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat">{{$penduduk->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>RT / RW</label>
                        <input type="text" name="rt_rw" class="form-control" value="{{$penduduk->rt_rw}}">
                    </div>
                    <div class="form-group">
                        <label>Kelurahan</label>
                        <input type="text" name="kelurahan" class="form-control" value="{{$penduduk->kelurahan}}">
                    </div>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <input type="text" name="kecamatan" class="form-control" value="{{$penduduk->kecamatan}}">
                    </div>
                    <div class="form-group">
                        <label>Agama</label>
                        <select name="agama" class="form-control">
                            <option value="Islam" {!! ($penduduk->agama == 'Islam') ? 'selected' : '' !!}>Islam</option>
                            <option value="Kristen" {!! ($penduduk->agama == 'Kristen') ? 'selected' : '' !!}>Kristen</option>
                            <option value="Hindu" {!! ($penduduk->agama == 'Hindu') ? 'selected' : '' !!}>Hindu</option>
                            <option value="Buddha" {!! ($penduduk->agama == 'Buddha') ? 'selected' : '' !!}>Buddha</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status Perkawinan</label>
                        <input type="text" name="status_perkawinan" class="form-control" value="{{$penduduk->status_perkawinan}}">
                    </div>
                    <div class="form-group">
                        <label>Pekerjaan</label>
                        <input type="text" name="pekerjaan" class="form-control" value="{{$penduduk->pekerjaan}}">
                    </div>
                    <div class="form-group">
                        <label>Kewarganegaraan</label>
                        <input type="text" name="kewarganegaraan" class="form-control" value="{{$penduduk->kewarganegaraan}}">
                    </div>
                    <div class="form-group">
                        <label>Berlaku Hingga</label>
                        <input type="text" name="berlaku_hingga" class="form-control" value="{{$penduduk->berlaku_hingga}}">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection