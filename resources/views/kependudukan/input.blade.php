@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Input Kartu Keluarga</h4>
                <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p>
            </div>
            <div class="card-body ">
                <form method="post" action="{{ url('kependudukan/insert') }}">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Nomor KK</label>
                        <select name="nikk" class="form-control">
                            <option selected="" disabled="" value="">-- Pilih Nomor KK --</option>
                            @foreach($kk as $k)
                                <option value="{{$k->nikk}}">{{$k->nikk}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>NIK</label>
                        <input type="text" name="nik" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label><br>
                        <input type="radio" name="jenis_kelamin" value="L" class=""> Laki-Laki<br>
                        <input type="radio" name="jenis_kelamin" value="P" class=""> Perempuan
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat"></textarea>
                    </div>
                    <div class="form-group">
                        <label>RT / RW</label>
                        <input type="text" name="rt_rw" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Kelurahan</label>
                        <input type="text" name="kelurahan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <input type="text" name="kecamatan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Agama</label>
                        <select name="agama" class="form-control">
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status Perkawinan</label>
                        <input type="text" name="status_perkawinan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Pekerjaan</label>
                        <input type="text" name="pekerjaan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Kewarganegaraan</label>
                        <input type="text" name="kewarganegaraan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Berlaku Hingga</label>
                        <input type="text" name="berlaku_hingga" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection