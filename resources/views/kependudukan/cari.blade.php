@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Cari Nomor Induk Kependudukan</h4>
                <!-- <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p> -->
            </div>
            <div class="card-body ">
                <form action="{{url('carinik/result')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Nomor Induk Kependudukan</label>
                        <input type="text" name="nik" class="form-control" required="">
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary pull-right" value="Cari">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    
</div>
@endsection