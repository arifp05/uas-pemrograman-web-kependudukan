<footer class="footer">
    <div class="container">
        <nav>
            <ul class="footer-menu">
                <li>
                    <a href="{{url('kk')}}">
                        Daftar KK
                    </a>
                </li>
                <li>
                    <a href="{{url('kependudukan')}}">
                        Daftar Kependudukan
                    </a>
                </li>
                <li>
                    <a href="{{url('carikk')}}">
                        Cari KK
                    </a>
                </li>
                <li>
                    <a href="{{url('carinik')}}">
                        Cari NIK
                    </a>
                </li>
            </ul>
            <p class="copyright text-center">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                Widyatama University, made with <i class="fa fa-heart" style="color: pink"></i> for a better future
            </p>
        </nav>
    </div>
</footer>