<div class="sidebar" data-color="blue" data-image="{{ asset('assets/img/sidebar-5.jpg') }}">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{url('/')}}" class="simple-text">
                Pusat Kependudukan
            </a>
        </div>
        <ul class="nav">
            <li class="{!! (Request::is("/") ? 'nav-item active' : (Request::is("/*") ? 'nav-item active' : '')) !!}">
                <a class="nav-link" href="{{ url('/') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{!! (Request::is("kk") ? 'nav-item active' : (Request::is("kk/*") ? 'nav-item active' : '')) !!}">
                <a class="nav-link" href="{{ url('kk') }}">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>Daftar KK</p>
                </a>
            </li>
            <li class="{!! (Request::is("kependudukan") ? 'nav-item active' : (Request::is("kependudukan/*") ? 'nav-item active' : '')) !!}">
                <a class="nav-link" href="{{ url('kependudukan') }}">
                    <i class="nc-icon nc-credit-card"></i>
                    <p>Daftar Kependudukan</p>
                </a>
            </li>

            <li class="{!! (Request::is("carikk") ? 'nav-item active' : (Request::is("carikk/*") ? 'nav-item active' : '')) !!}">
                <a class="nav-link" href="{{ url('carikk') }}">
                    <i class="nc-icon nc-zoom-split"></i>
                    <p>Cari KK</p>
                </a>
            </li>
            <li class="{!! (Request::is("carinik") ? 'nav-item active' : (Request::is("carinik/*") ? 'nav-item active' : '')) !!}">
                <a class="nav-link" href="{{ url('carinik') }}">
                    <i class="nc-icon nc-zoom-split"></i>
                    <p>Cari NIK</p>
                </a>
            </li>
            
        </ul>
    </div>
</div>