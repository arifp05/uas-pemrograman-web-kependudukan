@include("layouts.partials.head")

<body>
    <div class="wrapper">
        @include("layouts.partials.sidebar")
        <div class="main-panel">
            @include("layouts.partials.navbar")
            <div class="content">
                <div class="container-fluid">
                    @yield("content")
                </div>
            </div>
            @include("layouts.partials.footer")
        </div>
    </div>
</body>
@yield("modals")
@include("layouts.partials.script")
@yield("scripts")

</html>