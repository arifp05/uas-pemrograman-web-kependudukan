@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Input Kartu Keluarga</h4>
                <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p>
            </div>
            <div class="card-body ">
                <form action="{{url('kk/insert')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Nomor Induk Kartu Keluarga</label>
                        <input type="text" name="nikk" class="form-control" required="">
                    </div>
                    <div class="form-group">
                        <label>NIK Kepala Keluarga (Kosongkan jika NIK belum diinputkan)</label>
                        <select name="nik" class="form-control">
                            <option selected="" value="0">Pilih ini jika NIK belum diinputkan</option>
                            @foreach($penduduk as $p)
                                <option value="{{$p->nik}}">{{$p->nik}} - {{$p->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <div class="card-footer ">
                <div class="legend">
                    <i class="fa fa-plus text-success"></i> Tambah Anggota Keluarga
                    <i class="fa fa-trash text-danger"></i> Hapus
                </div>
                <hr>
                <div class="stats">
                    <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
</div>
@endsection