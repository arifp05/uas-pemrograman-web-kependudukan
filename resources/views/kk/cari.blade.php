@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Cari Nomor Kartu Keluarga</h4>
                <!-- <p class="card-category">Input kartu keluarga beserta kepala kelurganya</p> -->
            </div>
            <div class="card-body ">
                <form action="{{url('carikk/result')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Nomor Induk Kartu Keluarga</label>
                        <input type="text" name="nikk" class="form-control" required="">
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="" class="btn btn-primary pull-right" value="Cari">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    @if(isset($kk))
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Hasil Pencarian Kartu Keluarga</h4>
                <!-- <p class="card-category">Daftar kartu keluarga beserta kepala kelurganya</p> -->
            </div>
            <div class="card-body ">
                <div class="card-body table-full-width table-responsive">
                    <!-- <a href="{{ url('kk/input') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah KK</a> -->
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Nomor KK</th>
                            <th>Nama Kepala Keluarga</th>
                            <th>Jumlah Anggota Keluarga</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($kk as $k)
                            @php
                                $jumlah_keluarga = App\Model\Penduduk::where("nikk", $k->nikk)->count();
                            @endphp
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$k->nikk}}</td>
                                <td>{{ isset($k->kepala->nama) ? $k->kepala->nama : '-'}}</td>
                                <td>{{$jumlah_keluarga}} Orang</td>
                                <td><a href="{{ url('kk/edit') }}/{{$k->nikk}}" class=" btn-success btn-xs"><i class="fa fa-user" style="color: white"></i></a>&nbsp;<button data-id="{{$k->nikk}}" class=" btn-danger btn-xs open-del"><i class="fa fa-trash" style="color: white"></i></button></td>
                            </tr>
                            @php 
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer ">
                <div class="legend">
                    <i class="fa fa-user text-success"></i> Tetapkan Kepala Keluarga
                    <i class="fa fa-trash text-danger"></i> Hapus
                </div>
                <hr>
                <div class="stats">
                    <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection