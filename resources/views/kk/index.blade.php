@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Daftar Kartu Keluarga</h4>
                <p class="card-category">Daftar kartu keluarga beserta kepala kelurganya</p>
            </div>
            <div class="card-body ">
                <div class="card-body table-full-width table-responsive">
                    <a href="{{ url('kk/input') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah KK</a>
                    <table class="table table-hover table-striped">
                        <thead>
                            <th>#</th>
                            <th>Nomor KK</th>
                            <th>Nama Kepala Keluarga</th>
                            <th>Jumlah Anggota Keluarga</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            @php 
                                $no = 1;
                            @endphp
                            @foreach($kk as $k)
                            @php
                                $jumlah_keluarga = App\Model\Penduduk::where("nikk", $k->nikk)->count();
                            @endphp
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$k->nikk}}</td>
                                <td>{{ isset($k->kepala->nama) ? $k->kepala->nama : '-'}}</td>
                                <td>{{$jumlah_keluarga}} Orang</td>
                                <td><a href="{{ url('kk/detail') }}/{{$k->nikk}}" class=" btn-primary btn-xs"><i class="fa fa-search" style="color: white"></i></a>&nbsp;<a href="{{ url('kk/edit') }}/{{$k->nikk}}" class=" btn-success btn-xs"><i class="fa fa-user" style="color: white"></i></a>&nbsp;<button data-id="{{$k->nikk}}" class=" btn-danger btn-xs open-del"><i class="fa fa-trash" style="color: white"></i></button></td>
                            </tr>
                            @php 
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer ">
                <div class="legend">
                    <i class="fa fa-user text-success"></i> Tetapkan Kepala Keluarga
                    <i class="fa fa-trash text-danger"></i> Hapus
                </div>
                <hr>
                <div class="stats">
                    <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("modals")
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->

            <div class="modal-header">
                <h4 class="modal-title">Hapus KK</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form method="post" action="{{url('kk/delete')}}">
                <!-- Modal body -->
                {!! csrf_field() !!}
                <div class="modal-body">
                Apakah Anda yakin akan menghapus nomor KK ini ? Semua NIK di bawah KK ini juga akan secara otomatis terhapus.
                <input type="hidden" name="id" id="nik_modal">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script type="text/javascript">
    $(document).ready(function(){
        $(".open-del").click(function(){
            var id = $(this).data("id");
            $("#myModal").modal("show");
            $("#nik_modal").val(id);
        });
    });

function deleteModal(id){
    
    $("#myModal").modal("show");
    $("#nik_modal").val(id);
}
</script>
@endsection