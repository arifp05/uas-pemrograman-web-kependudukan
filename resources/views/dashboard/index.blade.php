@extends("layouts.application")

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Halaman Ini Bersifat Preview Saja</h4>
                <p class="card-category">Untuk Melihat dan Mengoperasikan Data, Klik Pada Navigasi Di Panel Kiri</p>
                <br>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Statistik Kependudukan</h4>
                <p class="card-category">Hasil Survey Terakhir</p>
            </div>
            <div class="card-body ">
                <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
            </div>
            <div class="card-footer ">
                <div class="legend">
                    <i class="fa fa-circle text-info"></i> Pria
                    <i class="fa fa-circle text-danger"></i> Wanita
                    <i class="fa fa-circle text-warning"></i> Belum Tedata
                </div>
                <hr>
                <div class="stats">
                    <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Pertumbuhan Data Penduduk </h4>
                <p class="card-category">Dalam 24 Jam Terakhir</p>
            </div>
            <div class="card-body ">
                <div id="chartHours" class="ct-chart"></div>
            </div>
            <div class="card-footer ">
                <div class="legend">
                    <i class="fa fa-circle text-info"></i> Open
                    <i class="fa fa-circle text-danger"></i> Click
                    <i class="fa fa-circle text-warning"></i> Click Second Time
                </div>
                <hr>
                <div class="stats">
                    <i class="fa fa-history"></i> Updated 3 minutes ago
                </div>
            </div>
        </div>
    </div>
</div>


@endsection