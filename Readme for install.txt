Cara Install Melalui Download Email : 
1. Download project
2. Extract di lokasi web server lokal Anda (htdocs)
3. Import DB, DB SQL terletak di  database/kependudukan.sql, namakan database dengan nama kependudukan
4. Ubah konfigurasi dengan mengedit file .env pada bagian :
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=kependudukan
	DB_USERNAME=homestead
	DB_PASSWORD=secret

Cara Install Melalui Git : 
1. Clone repository git yang ada di lokasi web server Anda (htdocs)
2. Buka command prompt, arahkan ke folder project yang sudah diclone
3. Masukkan perintah sebagai berikut : 
	- composer install 
	- sudo chmod -R 777 storage/
	- sudo chmod -R 777 bootstrap/
	- cp .env.example .env (Jika Linux) / expand .env.example .env (Jika Windows)
4. Import DB, DB SQL terletak di  database/kependudukan.sql, namakan database dengan nama kependudukan
5. Ubah konfigurasi dengan mengedit file .env pada bagian :
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=kependudukan
	DB_USERNAME=homestead
	DB_PASSWORD=secret